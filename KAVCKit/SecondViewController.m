//
//  SecondViewController.m
//  KAVCKit
//
//  Created by Anton Kovalev on 04.04.16.
//  Copyright © 2016 Rosberry. All rights reserved.
//

#import "SecondViewController.h"
#import "KAViewControllersKit.h"

@interface SecondViewController ()

@end

@implementation SecondViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    
//    [self setTranslucentNavigationBar:YES];
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

- (void)viewWillAppear:(BOOL)animated {
    [super viewWillAppear:animated];
    
    [self.ka_navigationController setNavigationBarHidden:NO animated:NO];
}

- (void)viewDidAppear:(BOOL)animated {
    [super viewDidAppear:animated];
    
//    [self.customNavigationController setNavigationBarHidden:NO animated:YES];
}

@end
