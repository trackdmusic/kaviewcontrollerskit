//
//  CustomNavigationBar.m
//  KAVCKit
//
//  Created by Anton Kovalev on 01.04.16.
//  Copyright © 2016 Rosberry. All rights reserved.
//

#import "CustomNavigationBar.h"
#import "KAViewControllersKit.h"

@interface CustomNavigationBar ()

@property (nonatomic) IBOutlet UIButton *backButton;
@property (nonatomic) IBOutlet UIView *leftButtonsView;
@property (nonatomic) IBOutlet UILabel *helpTitleLabel;
@property (nonatomic) IBOutlet UILabel *titleLabel;
@property (nonatomic) IBOutlet UIView *rightButtonsView;

@end

@implementation CustomNavigationBar

- (instancetype)init {
    UINib *nib = [UINib nibWithNibName:NSStringFromClass([self class]) bundle:nil];
    NSArray *objects = [nib instantiateWithOwner:nil options:nil];
    for (id obj in objects) {
        if ([obj isKindOfClass:[self class]]) {
            self = (CustomNavigationBar *)obj;
            return self;
        }
    }
    return [super init];
}

- (void)needShowBackButtonWithAnimationDuration:(double)duration fromViewController:(UIViewController *)fromVC toViewController:(UIViewController *)toVC {
    if (duration) {
        [UIView animateWithDuration:duration animations:^{
            [self.backButton setAlpha:1];
        }];
    } else {
        [self.backButton setAlpha:1];
    }
}

- (void)needHideBackButtonWithAnimationDuration:(double)duration fromViewController:(UIViewController *)fromVC toViewController:(UIViewController *)toVC {
    if (duration) {
        [UIView animateWithDuration:duration animations:^{
            [self.backButton setAlpha:0];
        }];
    } else {
        [self.backButton setAlpha:0];
    }
}

- (void)needUpdateWithTransitionType:(KANavigationControllerTransitionType)type withAnimationDuration:(double)duration fromViewController:(UIViewController *)fromVC toViewController:(UIViewController *)toVC {
    [self.helpTitleLabel setText:fromVC.title];
    [self.titleLabel setText:toVC.title];
    
    if (duration) {
        [self addViewToRight:toVC.ka_rightNavigationItem];
        [self addViewToLeft:toVC.ka_leftNavigationItem];
        
        [self.helpTitleLabel setAlpha:1.f];
        [self.titleLabel setAlpha:.0f];
        [UIView animateWithDuration:duration animations:^{
            [self.helpTitleLabel setAlpha:.0f];
            [self.titleLabel setAlpha:1.f];
        } completion:^(BOOL finished) {
            
        }];
    } else {
        [self.helpTitleLabel setAlpha:.0f];
        [self.titleLabel setAlpha:1.f];
    }
}

- (IBAction)backButtonPressed:(UIButton *)sender {
    [self backButtonPressed];
}

- (void)addViewToRight:(UIView *)view {
    for (UIView *sv in self.rightButtonsView.subviews) {
        [sv removeFromSuperview];
    }
    if (!view){
//        if (self.titleLabelsRightConstraint){
//            [self removeConstraint:self.titleLabelsRightConstraint];
//            self.titleLabelsRightConstraint = nil;
//        }
        return;
    }
    
    [self.rightButtonsView addSubview:view];
    
    NSDictionary *views = NSDictionaryOfVariableBindings(view);
    NSArray *ch = [NSLayoutConstraint constraintsWithVisualFormat:@"H:|[view]|" options:0 metrics:nil views:views];
    NSArray *cv = [NSLayoutConstraint constraintsWithVisualFormat:@"V:|[view]|" options:0 metrics:nil views:views];
    [self.rightButtonsView addConstraints:[ch arrayByAddingObjectsFromArray:cv]];
}

- (void)addViewToLeft:(UIView *)view{
    for (UIView *sv in self.leftButtonsView.subviews) {
        if (![sv isEqual:self.backButton]) {
            [sv removeFromSuperview];
        }
    }
    if (!view)
        return;
    
    self.backButton.alpha = 0;
    [self.leftButtonsView addSubview:view];
    
    NSDictionary *views = NSDictionaryOfVariableBindings(view);
    NSArray *ch = [NSLayoutConstraint constraintsWithVisualFormat:@"H:|[view]|" options:0 metrics:nil views:views];
    NSArray *cv = [NSLayoutConstraint constraintsWithVisualFormat:@"V:|[view]|" options:0 metrics:nil views:views];
    [self.leftButtonsView addConstraints:[ch arrayByAddingObjectsFromArray:cv]];

}

@end
