//
//  UIView+UIScrollViewInsets.m
//  Collaborate
//
//  Created by Anton Kovalev on 27.01.16.
//  Copyright © 2016 Rosberry. All rights reserved.
//

#import "UIView+UIScrollViewInsets.h"

@implementation UIView (UIScrollViewInsets)

- (void)setScrollViewInsets:(UIEdgeInsets)insets {
    if ([self isKindOfClass:[UIScrollView class]]) {
        UIScrollView *scroll = (id)self;
        [scroll setContentInset:insets];
        [scroll setScrollIndicatorInsets:insets];
    } else if ([self isKindOfClass:[UIWebView class]]) {
        UIScrollView *scroll = ((UIWebView*)self).scrollView;
        [scroll setContentInset:insets];
        [scroll setScrollIndicatorInsets:insets];
    }
}

@end
