//
//  UIView+UIScrollViewInsets.h
//  Collaborate
//
//  Created by Anton Kovalev on 27.01.16.
//  Copyright © 2016 Rosberry. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface UIView (UIScrollViewInsets)

- (void)setScrollViewInsets:(UIEdgeInsets)insets;

@end
