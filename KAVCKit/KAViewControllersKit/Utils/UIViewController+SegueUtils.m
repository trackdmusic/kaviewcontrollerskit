//
//  UIViewController+SegueUtils.m
//  Collaborate
//
//  Created by Anton on 11.06.15.
//  Copyright (c) 2015 Rosberry. All rights reserved.
//

#import "UIViewController+SegueUtils.h"

@implementation UIViewController (SegueUtils)

- (BOOL)canPerformSegueWithIdentifier:(NSString *)identifier{
    NSPredicate *p = [NSPredicate predicateWithFormat:@"identifier==%@", identifier];
    return [[self valueForKey:@"storyboardSegueTemplates"] filteredArrayUsingPredicate:p].count > 0;
}

@end
