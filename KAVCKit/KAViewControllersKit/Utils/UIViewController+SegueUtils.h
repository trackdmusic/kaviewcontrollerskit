//
//  UIViewController+SegueUtils.h
//  Collaborate
//
//  Created by Anton on 11.06.15.
//  Copyright (c) 2015 Rosberry. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface UIViewController (SegueUtils)

- (BOOL)canPerformSegueWithIdentifier:(NSString *)identifier;

@end
