//
//  KANavigationController.h
//  Collaborate
//
//  Created by Anton on 20.01.15.
//  Copyright (c) 2015 Rosberry. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "KANavigationBar.h"
#import "KAViewControllerContextTransitioning.h"
@class KANavigationController, KANavigationControllerCommonContainerView;

@protocol KANavigationControllerDelegate <NSObject>

@optional
/*!
 * @discussion Use this delegate method to return non-interactive transition animator which is used to make custom animation for transition betwen controllers.
 * @param navigationController The navigation controller whose navigation stack is changing.
 * @param transitionType Type of transition in navigation controller.
 * @return The animator object responsible for managing the transition animations, or nil if you want to use the standard navigation controller transitions.
 */
- (id<UIViewControllerAnimatedTransitioning>)navigationController:(KANavigationController *)navigationController animationControllerForTransition:(KANavigationControllerTransitionType)transitionType fromViewController:(UIViewController *)fromVC toViewController:(UIViewController *)toVC;
/*!
 * @discussion Use this delegate method to return interactive controller for transition animator which is used to make custom animation for transition betwen controllers.
 * @param navigationController The navigation controller whose navigation stack is changing.
 * @param animationController The noninteractive animator object provided by the delegate’s navigationController:animationControllerForTransition:fromViewController:toViewController: method.
 * @return The animator object responsible for managing the transition animations, or nil if you want to use the standard navigation controller transitions.
 */
- (id<UIViewControllerInteractiveTransitioning>)navigationController:(KANavigationController *)navigationController interactiveControllerForAnimationController:(id<UIViewControllerAnimatedTransitioning>)animationController;

@required
/*!
 * @discussion This delegate method will be called when navigation controller will need navigation bar
 * @param navigationController The navigation controller asking for navigation bar
 * @return Instance of KANavigationBar class
 */
- (KANavigationBar *)navigationBarForNavigationController:(KANavigationController *)navigationController;
/*!
 * @discussion This delegate method will be called when navigation controller will need size for navigation bar
 * @param navigationController The navigation controller asking for size
 * @param navigationBar The navigation bar
 * @return CGSize
 */
- (CGSize)navigationController:(KANavigationController *)navigationController sizeForNavigationBar:(KANavigationBar *)navigationBar;

@end

@interface KANavigationController : UIViewController

///The view controller at the top of the navigation stack. (read-only)
@property (nonatomic, strong, readonly) UIViewController *topViewController;
///The view controller at the bottom of the navigation stack. (read-only)
@property (nonatomic, strong, readonly) UIViewController *rootViewController;
///The view controllers currently on the navigation stack.
@property (nonatomic, strong) NSArray *viewControllers;
///Current navigation bar.
@property (nonatomic, strong, readonly) KANavigationBar *navigationBar;
/*!
 * @discussion The delegate of the navigation controller object.
 * @warning Should be set before viewDidLoad method
 */
@property (nonatomic, weak) id<KANavigationControllerDelegate> delegate;
///Current container view for all views
@property (nonatomic, strong, readonly) KANavigationControllerCommonContainerView *commonContainerView;
///Gesture recognizer for interactive popping view controller
@property (nonatomic, readonly) UIGestureRecognizer *interactivePopGestureRecognizer;
///Set it to NO if you don'w want interactive popping
@property (nonatomic, assign) BOOL interactivePopGestureEnabled;

///Use it if you need to know what was the type of last transition
@property (nonatomic) KANavigationControllerTransitionType lastTransitionType;
///Check if lastTransitionType already used
@property (nonatomic) BOOL lastTransitionTypeUsed;

/*!
 * @discussion Initializes and returns a newly created navigation controller.
 * @param vc The view controller that resides at the bottom of the navigation stack.
 * @return The initialized navigation controller object or nil if there was a problem initializing the object.
 */
- (instancetype)initWithRootViewController:(UIViewController *)vc;
///Pushes a view controller onto the receiver’s stack and updates the display.
- (void)pushViewController:(UIViewController *)vc animated:(BOOL)animated completion:(void(^)())completion;
///Pops the top view controller from the navigation stack and updates the display.
- (void)popViewControllerAnimated:(BOOL)animated completion:(void(^)())completion;
///Pops all the view controllers on the stack except the root view controller and updates the display.
- (void)popToRootViewControllerAnimated:(BOOL)animated completion:(void (^)())completion;
///Pops view controllers until the specified view controller is at the top of the navigation stack.
- (void)popToViewController:(UIViewController *)vc animated:(BOOL)animated completion:(void (^)())completion;
///Sets whether the navigation bar is hidden.
- (void)setNavigationBarHidden:(BOOL)hidden animated:(BOOL)animated;
/*!
 * @discussion Replaces the view controllers currently managed by the navigation controller with the specified items.
 Use this method to update or replace the current view controller stack without pushing or popping each controller explicitly. In addition, this method lets you update the set of controllers without animating the changes, which might be appropriate at launch time when you want to return the navigation controller to a previous state.
 * @param viewControllers The view controllers to place in the stack. The front-to-back order of the controllers in this array represents the new bottom-to-top order of the controllers in the navigation stack. Thus, the last item added to the array becomes the top item of the navigation stack.
 */
- (void)setViewControllers:(NSArray *)viewControllers animated:(BOOL)animated;

///Helper method for getting right frame for view controller.
- (CGRect)frameForViewController:(UIViewController *)viewController withNavigationBarHidden:(BOOL)navigationBarHidden;
///Helper method for getting right frame for navigation bar.
- (CGRect)frameForNavigationBarHidden:(BOOL)hidden inViewController:(UIViewController *)viewController;

@end


extern NSString * const customNavigationSegueIdentifierBaseString;
@interface CustomNavigationRootViewControllerSegue : UIStoryboardSegue
@end


@interface CustomNavigationPushViewControllerSegue : UIStoryboardSegue
@end


@interface UIViewController (NavigationController)

/*!
 * @discussion The nearest ancestor in the view controller hierarchy that is a navigation controller. (read-only)
 If the view controller or one of its ancestors is a child of a navigation controller, this property contains the owning navigation controller. This property is nil if the view controller is not embedded inside a navigation controller.
 */
@property (nonatomic, weak) KANavigationController *ka_navigationController;
///A custom view displayed on the left edge of the navigation bar when the receiver is the top navigation item.
@property (nonatomic, strong) UIView *ka_leftNavigationItem;
///A custom view displayed on the right edge of the navigation bar when the receiver is the top navigation item.
@property (nonatomic, strong) UIView *ka_rightNavigationItem;
///Set this property to YES if you want translucentNavigationBar behavior for views layout
@property (nonatomic) IBInspectable BOOL translucentNavigationBar;
///Set this property to YES if you want top level scrollviews adjust their insets automatically 
@property (nonatomic) IBInspectable BOOL ka_automaticallyAdjustsScrollViewInsets;

@end
