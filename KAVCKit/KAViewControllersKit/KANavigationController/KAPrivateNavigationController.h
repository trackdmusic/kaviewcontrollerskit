//
//  KAPrivateNavigationController.h
//  KAVCKit
//
//  Created by Anton Kovalev on 19.08.16.
//  Copyright © 2016 Rosberry. All rights reserved.
//

#import "KANavigationController.h"

@interface KANavigationController (Private)

@property (nonatomic, readonly) BOOL navigationBarShouldBeHiddenOnAppearance;
@property (nonatomic, readonly) BOOL navigationBarInitiallyHidden;

- (void)ka_tabBarControllerDidStartTransition:(KATabBarController *)ka_tabBarController from:(UIViewController *)fromVC to:(UIViewController *)toVC;
- (void)ka_tabBarControllerDidFinishTransition:(KATabBarController *)ka_tabBarController from:(UIViewController *)fromVC to:(UIViewController *)toVC;
- (void)ka_tabBarControllerDidCancelTransition:(KATabBarController *)ka_tabBarController from:(UIViewController *)fromVC to:(UIViewController *)toVC;

@end
