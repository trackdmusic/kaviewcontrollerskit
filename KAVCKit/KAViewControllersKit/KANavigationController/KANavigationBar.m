//
//  KANavigationBar.m
//  Collaborate
//
//  Created by Anton on 21.01.15.
//  Copyright (c) 2015 Rosberry. All rights reserved.
//

#import "KANavigationBar.h"

@implementation KANavigationBar

- (void)needHideBackButtonWithAnimationDuration:(double)duration fromViewController:(UIViewController *)fromVC toViewController:(UIViewController *)toVC{
    
}

- (void)needShowBackButtonWithAnimationDuration:(double)duration fromViewController:(UIViewController *)fromVC toViewController:(UIViewController *)toVC{
    
}

- (void)needUpdateWithTransitionType:(KANavigationControllerTransitionType)type withAnimationDuration:(double)duration fromViewController:(UIViewController *)fromVC toViewController:(UIViewController *)toVC{
    
}

- (void)backButtonPressed {
    if ([self.delegate respondsToSelector:@selector(navigationBarBackButtonPressed:)])
        [self.delegate navigationBarBackButtonPressed:self];
}

@end
