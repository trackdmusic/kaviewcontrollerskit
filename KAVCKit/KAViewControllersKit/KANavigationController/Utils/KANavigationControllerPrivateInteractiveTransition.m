//
//  KANavigationControllerPrivateInteractiveTransition.m
//  KAVCKit
//
//  Created by Anton Kovalev on 12.04.16.
//  Copyright © 2016 Rosberry. All rights reserved.
//

#import "KANavigationControllerPrivateInteractiveTransition.h"
#import "KAViewControllersKit.h"

@implementation KANavigationControllerPrivateInteractiveTransition

- (instancetype)initWithGestureRecognizerInView:(UIView *)view recognizerBegan:(void (^)(UIScreenEdgePanGestureRecognizer *recognizer))beganBlock{
    self = [super init];
    if (self) {
        self.recognizerBeganBlock = beganBlock;
        self.recognizer = [[UIScreenEdgePanGestureRecognizer alloc] initWithTarget:self action:@selector(screenEdgePanned:)];
        [self.recognizer setEdges:UIRectEdgeLeft];
        [view addGestureRecognizer:self.recognizer];
    }
    return self;
}

- (void)screenEdgePanned:(UIScreenEdgePanGestureRecognizer *)recognizer{
    switch (recognizer.state) {
        case UIGestureRecognizerStateBegan:
            self.recognizerBeganBlock(recognizer);
            break;
            
        case UIGestureRecognizerStateChanged:{
            CGPoint translation = [recognizer translationInView:recognizer.view];
            CGFloat d = translation.x / CGRectGetWidth(recognizer.view.bounds);
            [self updateInteractiveTransition:d];
            break;
        }
            
        case UIGestureRecognizerStateEnded:{
            CGPoint velocity = [recognizer velocityInView:recognizer.view];
            if (self.percentComplete > 0.5 || velocity.x >= 250) {
                [self finishInteractiveTransition];
            } else {
                [self cancelInteractiveTransition];
            }
            break;
        }
            
        default:
            break;
    }
}

- (void)handleInteractiveTransitionCancelationWithContext:(id<KAViewControllerContextTransitioning>)context {
    if (context.willCancelInteractiveTransitionBlock) {
        context.willCancelInteractiveTransitionBlock();
    }
    UIViewController *fromVC = [context viewControllerForKey:UITransitionContextFromViewControllerKey];
    CGRect fromLayerFrame = [context initialFrameForViewController:fromVC];
    CALayer *fromLayer = fromVC.view.layer;
    
    KANavigationBar *navigationBar = (id)[context navigationBar];
    CGRect barFrame = [context initialFrameForNavigationBar];
    
    [CATransaction begin];
    [CATransaction setValue:@(YES) forKey:kCATransactionDisableActions];
    fromLayer.frame = fromLayerFrame;
    navigationBar.frame = barFrame;
    [CATransaction commit];
}

@end
