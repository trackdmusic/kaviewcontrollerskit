//
//  KANavigationControllerPrivateAnimatedTransition.h
//  KAVCKit
//
//  Created by Anton Kovalev on 12.04.16.
//  Copyright © 2016 Rosberry. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface KANavigationControllerPrivateAnimatedTransition : NSObject <UIViewControllerAnimatedTransitioning>

@end
