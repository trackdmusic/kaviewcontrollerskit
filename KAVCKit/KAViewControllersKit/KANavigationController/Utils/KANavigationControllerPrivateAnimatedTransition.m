//
//  KANavigationControllerPrivateAnimatedTransition.m
//  KAVCKit
//
//  Created by Anton Kovalev on 12.04.16.
//  Copyright © 2016 Rosberry. All rights reserved.
//

#import "KANavigationControllerPrivateAnimatedTransition.h"
#import "KAViewControllersKit.h"

static NSInteger const KAShadowViewTag = 1908763;

@implementation KANavigationControllerPrivateAnimatedTransition

- (NSTimeInterval)transitionDuration:(id<UIViewControllerContextTransitioning>)transitionContext {
    return .4;
}

- (void)animateTransition:(id<KAViewControllerContextTransitioning>)transitionContext{
    UIViewController *toViewController = [transitionContext viewControllerForKey:UITransitionContextToViewControllerKey];
    UIViewController *fromViewController = [transitionContext viewControllerForKey:UITransitionContextFromViewControllerKey];
    
    [transitionContext startTransitionWithDuration:[self transitionDuration:transitionContext]];
    UIView *containerView = [transitionContext containerView];
    
    BOOL navigationBarShouldBeHidden = [transitionContext navigationBarShouldBeHidden];
    BOOL navBarInitiallyHidden = [transitionContext navigationBarInitiallyHidden];
    
    KANavigationBar *navigationBar = (KANavigationBar *)transitionContext.navigationBar;
    
    double rightOffset = CGRectGetWidth(containerView.bounds) * .3f;
    
    CGRect frame = [transitionContext finalFrameForViewController:toViewController];
    
    CGRect nviewNewFrame = frame;
    nviewNewFrame.origin.x = 0;
    
    UIView *oview = fromViewController.view;
    UIView *nview = toViewController.view;
    KANavigationControllerTransitionType type = [transitionContext transitionType];
    
    [containerView addSubview:nview];
    
    CGRect oviewNewFrame = oview.frame;
    oviewNewFrame.origin.x = CGRectGetWidth(oview.bounds);
    
    double transitionDuration = [self transitionDuration:transitionContext];
    
    if (type == KANavigationControllerTransitionTypePush) {
        frame.origin.x = CGRectGetWidth(nview.bounds);
        [nview setFrame:frame];
        oviewNewFrame.origin.x = -rightOffset;
        
        if (!navigationBarShouldBeHidden) {
            [containerView sendSubviewToBack:nview];
            [containerView bringSubviewToFront:navigationBar];
        } else {
            [containerView sendSubviewToBack:navigationBar];
            [containerView bringSubviewToFront:nview];
        }
        
        [containerView sendSubviewToBack:oview];
        
        [self addShadowToView:nview withTransitionType:type andDuration:transitionDuration];
    } else if (type == KANavigationControllerTransitionTypePop) {
        frame.origin.x = -rightOffset;
        [nview setFrame:frame];
        
        if (!navBarInitiallyHidden) {
            [containerView sendSubviewToBack:oview];
            [containerView bringSubviewToFront:navigationBar];
        } else {
            [containerView bringSubviewToFront:oview];
            [containerView sendSubviewToBack:navigationBar];
        }
        
        [containerView sendSubviewToBack:nview];
        
        [self addShadowToView:oview withTransitionType:type andDuration:transitionDuration];
    }
    
    [navigationBar setFrame:[transitionContext initialFrameForNavigationBar]];
    CGRect navBarFrame = [transitionContext finalFrameForNavigationBar];
    if (!fromViewController.translucentNavigationBar) {
        [self addShadowToView:navigationBar withTransitionType:type andDuration:transitionDuration];
    }
    
    if (transitionContext.isAnimated) {
        UIView *fadeOutView = [self animateFadeOutInView:containerView betweenOldView:oview andNewView:nview withTransitionType:type andDuration:transitionDuration];
        
        [UIView animateWithDuration:transitionDuration delay:.0f options:0x00 animations:^{
            [nview setFrame:nviewNewFrame];
            [oview setFrame:oviewNewFrame];
            [navigationBar setFrame:navBarFrame];
        } completion:^(BOOL finished) {
            [transitionContext completeTransition:![transitionContext transitionWasCancelled]];
            [fadeOutView removeFromSuperview];
            [self removeShadowFromView:oview];
            [self removeShadowFromView:nview];
            [self removeShadowFromView:navigationBar];
        }];
    } else {
        [oview setFrame:oviewNewFrame];
        [nview setFrame:nviewNewFrame];
        [navigationBar setFrame:navBarFrame];
        [transitionContext completeTransition:![transitionContext transitionWasCancelled]];
        [self removeShadowFromView:oview];
        [self removeShadowFromView:nview];
        [self removeShadowFromView:navigationBar];
    }
}

- (void)addShadowToView:(UIView *)view withTransitionType:(KANavigationControllerTransitionType)type andDuration:(double)duration {
    NSBundle *mainBundle = [NSBundle bundleForClass:[self class]];
    NSString *bundlePath = [mainBundle pathForResource:@"Resources" ofType:@"bundle"];
    NSBundle *bundle = [NSBundle bundleWithPath:bundlePath];
    
    UIImage *shadow = [UIImage imageNamed:@"shadow" inBundle:bundle compatibleWithTraitCollection:nil];
    shadow = [shadow imageWithRenderingMode:UIImageRenderingModeAlwaysTemplate];
    shadow = [shadow resizableImageWithCapInsets:UIEdgeInsetsMake(0, 0, 0, 0) resizingMode:UIImageResizingModeStretch];
    
    UIImageView *iv = [[UIImageView alloc] initWithImage:shadow];
    [iv setTintColor:[UIColor colorWithRed:0 green:0 blue:0 alpha:1]];
    [view addSubview:iv];
    [iv setTag:KAShadowViewTag];
    
    BOOL push = type == KANavigationControllerTransitionTypePush;
    
    CGRect bigFrame = CGRectMake(-1.5 * shadow.size.width, 0, 1.5 * shadow.size.width, CGRectGetHeight(view.bounds));
    CGRect smallFrame = CGRectMake(0, 0, 0, CGRectGetHeight(view.bounds));
    CGRect startFrame = bigFrame;
    CGRect endFrame = smallFrame;
    
    double delay = push ? duration * .65 : duration * .75;
    double shadowDuration = push ? duration * .2 : duration * .1;
    [iv.layer setFrame:startFrame];
    
    CABasicAnimation *animationBounds = [CABasicAnimation animationWithKeyPath:@"bounds"];
    animationBounds.fromValue = [NSValue valueWithCGRect:startFrame];
    animationBounds.toValue = [NSValue valueWithCGRect:endFrame];
    animationBounds.fillMode = kCAFillModeBackwards;
    animationBounds.duration = shadowDuration;
    animationBounds.beginTime = [iv.layer convertTime:CACurrentMediaTime() fromLayer:nil] + delay;
    animationBounds.removedOnCompletion = YES;
    [iv.layer addAnimation:animationBounds forKey:@"shadow.bounds"];
    
    CGRect frame = startFrame;
    
    CGPoint rightPoint = CGPointMake(-CGRectGetWidth(frame)/2.0, CGRectGetHeight(frame)/2.0);
    CGPoint leftPoint = CGPointMake(0, CGRectGetHeight(frame)/2.0);
    CGPoint startPoint = rightPoint;
    CGPoint endPoint = leftPoint;
    
    CABasicAnimation *animationPosition = [CABasicAnimation animationWithKeyPath:@"position"];
    animationPosition.fromValue = [NSValue valueWithCGPoint:startPoint];
    animationPosition.toValue = [NSValue valueWithCGPoint:endPoint];
    animationPosition.fillMode = kCAFillModeBackwards;
    animationPosition.duration = shadowDuration;
    animationPosition.beginTime = [iv.layer convertTime:CACurrentMediaTime() fromLayer:nil] + delay;
    animationPosition.removedOnCompletion = YES;
    [iv.layer addAnimation:animationPosition forKey:@"shadow.position"];
    
    [iv.layer setFrame:endFrame];
}

- (void)removeShadowFromView:(UIView *)view {
    UIImageView *iv = [view viewWithTag:KAShadowViewTag];
    [iv removeFromSuperview];
}

- (UIView *)animateFadeOutInView:(UIView *)containerView betweenOldView:(UIView *)oview andNewView:(UIView *)nview withTransitionType:(KANavigationControllerTransitionType)type andDuration:(double)duration {
    BOOL push = type == KANavigationControllerTransitionTypePush;
    
    double highAlpha = .25f;
    double lowAlpha = .0f;
    double startAlpha = push ? lowAlpha : highAlpha;
    double endAlpha = push ? highAlpha : lowAlpha;
    
    UIView *subview = push ? nview : oview;
    
    UIView *fadeOutView = [[UIView alloc] initWithFrame:containerView.frame];
    [fadeOutView setBackgroundColor:[UIColor blackColor]];
    [fadeOutView setAlpha:startAlpha];
    [containerView insertSubview:fadeOutView belowSubview:subview];
    
    double delay = push ? duration * .4 : duration * .2;
    double blackViewTransitionDuration = push ? duration * .6 : duration * .4;
    
    CABasicAnimation *animation = [CABasicAnimation animationWithKeyPath:@"opacity"];
    animation.fromValue = [NSNumber numberWithFloat:startAlpha];
    animation.toValue = [NSNumber numberWithFloat:endAlpha];
    animation.fillMode = kCAFillModeBackwards;
    animation.duration = blackViewTransitionDuration;
    animation.beginTime = [fadeOutView.layer convertTime:CACurrentMediaTime() fromLayer:nil] + delay;
    animation.removedOnCompletion = YES;
    [fadeOutView.layer addAnimation:animation forKey:@"fadeOut"];
    
    [fadeOutView.layer setOpacity:endAlpha];
    
    return fadeOutView;
}

- (void)dealloc {
    
}

@end
