//
//  KANavigationControllerPrivateInteractiveTransition.h
//  KAVCKit
//
//  Created by Anton Kovalev on 12.04.16.
//  Copyright © 2016 Rosberry. All rights reserved.
//

#import "AWPercentDrivenInteractiveTransition.h"

typedef void(^RecognizerBeganBlock)(UIScreenEdgePanGestureRecognizer *recognizer);

@interface KANavigationControllerPrivateInteractiveTransition : AWPercentDrivenInteractiveTransition

- (instancetype)initWithGestureRecognizerInView:(UIView *)view recognizerBegan:(void (^)(UIScreenEdgePanGestureRecognizer *recognizer))beganBlock;

@property (nonatomic, copy) RecognizerBeganBlock recognizerBeganBlock;
@property (nonatomic, strong) UIScreenEdgePanGestureRecognizer *recognizer;

@end
