//
//  KANavigationBar.h
//  Collaborate
//
//  Created by Anton on 21.01.15.
//  Copyright (c) 2015 Rosberry. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "KANavigationControllerTransitionType.h"
@protocol KANavigationBarDelegate;

@interface KANavigationBar : UIView

///This method will be called when back button in navigation bar should be showen.
- (void)needShowBackButtonWithAnimationDuration:(double)duration fromViewController:(UIViewController *)fromVC toViewController:(UIViewController *)toVC;
///This method will be called when back button in navigation bar should be hidden.
- (void)needHideBackButtonWithAnimationDuration:(double)duration fromViewController:(UIViewController *)fromVC toViewController:(UIViewController *)toVC;
///This method will be called when navigation bar should be updated.
- (void)needUpdateWithTransitionType:(KANavigationControllerTransitionType)type withAnimationDuration:(double)duration fromViewController:(UIViewController *)fromVC toViewController:(UIViewController *)toVC;
///Call this method for performing all back button related actions.
- (void)backButtonPressed;

///Do not override this property directly. Use setNavigationBarHidden:animated: method of KANavigationController instead.
@property (nonatomic, assign) BOOL hidden;
///Do not use this property.
@property (nonatomic, weak) id<KANavigationBarDelegate> delegate;

@end

@protocol KANavigationBarDelegate <NSObject>

///this method will be called when back button of navigation bar will be pressed.
- (void)navigationBarBackButtonPressed:(KANavigationBar *)navigationBar;

@end
