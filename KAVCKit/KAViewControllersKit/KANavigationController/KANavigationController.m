//
//  KANavigationController.m
//  Collaborate
//
//  Created by Anton on 20.01.15.
//  Copyright (c) 2015 Rosberry. All rights reserved.
//


#import "KANavigationController.h"
#import "AWPercentDrivenInteractiveTransition.h"
#import <objc/runtime.h>
#import "UIView+UIScrollViewInsets.h"
#import "KAPrivateTransitionContext.h"
#import "KANavigationControllerCommonContainerView.h"

#import "KANavigationControllerPrivateAnimatedTransition.h"
#import "KANavigationControllerPrivateInteractiveTransition.h"

#import "KATabBarController.h"
#import "UIViewController+SegueUtils.h"

#import "KAPrivateNavigationController.h"

@interface KANavigationController () <KANavigationBarDelegate>

@property (nonatomic, assign) BOOL inTransition;

@property (nonatomic, assign) BOOL navigationBarShouldBeHidden;
@property (nonatomic, assign) BOOL navigationBarShouldBeHiddenAnimated;

@property (nonatomic, assign) BOOL rootNeedPresent;
@property (nonatomic, assign) BOOL checkingNavBarHidden;

@property (nonatomic, strong) KANavigationControllerPrivateInteractiveTransition *privateInteractiveTransition;

@property (nonatomic, strong, readwrite) UIViewController *topViewController;
@property (nonatomic, strong, readwrite) UIViewController *rootViewController;
@property (nonatomic, strong, readwrite) KANavigationBar *navigationBar;
@property (nonatomic) NSArray *internalViewControllers;

@property (nonatomic) UIViewController *internalRootViewController;

@property (nonatomic, strong, readwrite) KANavigationControllerCommonContainerView *commonContainerView;

@property (nonatomic, readwrite) BOOL navigationBarShouldBeHiddenOnAppearance;
@property (nonatomic, readwrite) BOOL navigationBarInitiallyHidden;

@end

@implementation KANavigationController

- (void)dealloc{
    self.topViewController = nil;
    self.internalViewControllers = nil;
}

- (instancetype)initWithRootViewController:(UIViewController *)vc {
    self = [super init];
    if (self) {
        self.rootNeedPresent = YES;
        [self updateRootViewController:vc];
    }
    return self;
}

- (void)viewDidLoad {
    [super viewDidLoad];
    
    [self setupViewsStack];
    [self setupDefaultInteractiveTransition];
    
    [self loadRootViewControllerIfNeeded];
    [self displayRootViewControllerIfNeeded];
}

- (void)viewWillAppear:(BOOL)animated {
    [super viewWillAppear:animated];
    
    self.checkingNavBarHidden = YES;
    [self.topViewController beginAppearanceTransition:YES animated:NO];
    [self.topViewController.view setFrame:[self frameForViewController:self.topViewController withNavigationBarHidden:self.navigationBarShouldBeHidden]];
    [self.navigationBar setFrame:[self frameForNavigationBarHidden:self.navigationBarShouldBeHidden inViewController:self.topViewController]];
    self.checkingNavBarHidden = NO;
    [self.navigationBar setHidden:self.navigationBarShouldBeHidden];
}

- (void)viewDidAppear:(BOOL)animated {
    [super viewDidAppear:animated];
    
    self.checkingNavBarHidden = YES;
    [self.topViewController endAppearanceTransition];
    [self.topViewController.view setFrame:[self frameForViewController:self.topViewController withNavigationBarHidden:self.navigationBarShouldBeHidden]];
    CGRect frame = [self frameForNavigationBarHidden:self.navigationBarShouldBeHidden inViewController:self.topViewController];
    frame.origin.y = self.navigationBarShouldBeHidden ? -CGRectGetHeight(frame) : 0.f;
    if (self.navigationBarShouldBeHiddenAnimated) {
        [UIView animateWithDuration:.45f delay:.0f options:UIViewAnimationOptionBeginFromCurrentState|UIViewAnimationOptionCurveEaseInOut animations:^{
            [self.navigationBar setFrame:frame];
        } completion:^(BOOL finished) {
            [self.navigationBar setHidden:self.navigationBarShouldBeHidden];
        }];
    } else {
        [self.navigationBar setFrame:frame];
        [self.navigationBar setHidden:self.navigationBarShouldBeHidden];
    }
    self.checkingNavBarHidden = NO;
}

- (void)viewWillDisappear:(BOOL)animated {
    [super viewWillDisappear:animated];
    [self.topViewController beginAppearanceTransition:NO animated:animated];
}

- (void)viewDidDisappear:(BOOL)animated {
    [super viewDidDisappear:animated];
    [self.topViewController endAppearanceTransition];
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
}

- (void)viewDidLayoutSubviews {
    [super viewDidLayoutSubviews];
    
    [self.navigationBar layoutIfNeeded];
    
    CGRect frame = self.view.bounds;
    
    //BM: Another nasty Hack. Lets only change the frame of the bottom most commonContainer. Becuase, yeh, thats make "perfect" sense. Sigh.
    if (self.parentViewController == nil) {
        
        CGFloat topPadding = 0;
        CGFloat bottomPadding = 0;
        if (@available(iOS 11.0, *)) {
            UIWindow *window = UIApplication.sharedApplication.keyWindow;
            topPadding = window.safeAreaInsets.top;
            bottomPadding = window.safeAreaInsets.bottom;
        }
        
        frame.origin.y = topPadding;
        frame.size.height = frame.size.height - topPadding;
    }
    
    [self.commonContainerView setFrame:frame];
    
    if (!self.checkingNavBarHidden) {
        [self.navigationBar setFrame:[self frameForNavigationBarHidden:self.navigationBarShouldBeHidden inViewController:self.topViewController]];
        [self.commonContainerView bringSubviewToFront:self.navigationBar];
    }
}

#pragma mark - Setup

- (void)setupDefaultInteractiveTransition {
    __weak typeof(self) weakSelf = self;
    self.privateInteractiveTransition = [[KANavigationControllerPrivateInteractiveTransition alloc] initWithGestureRecognizerInView:self.commonContainerView recognizerBegan:^(UIScreenEdgePanGestureRecognizer *recognizer) {
        if (weakSelf.internalViewControllers.count > 1 && weakSelf.interactivePopGestureEnabled) {
            [weakSelf popViewControllerAnimated:YES completion:nil];
        }
    }];
}

- (void)setupViewsStack {
    self.navigationBar = [self requestNavigationBar];
    [self.navigationBar setDelegate:self];
    CGSize navBarSize = [self sizeForNavigationBar:self.navigationBar];
    [self.navigationBar setFrame:CGRectMake(0, 0, navBarSize.width, navBarSize.height)];
    
    self.view.clipsToBounds = YES;
    self.interactivePopGestureEnabled = YES;
    
    self.commonContainerView = [[KANavigationControllerCommonContainerView alloc] initWithFrame:self.view.bounds];
    [self deleteConstraintsForView:self.commonContainerView];
    [self.commonContainerView setBackgroundColor:[UIColor clearColor]];
    [self.view addSubview:self.commonContainerView];
    
    [self.commonContainerView addSubview:self.navigationBar];
}

#pragma mark - Methods

- (void)deleteConstraintsForView:(UIView *)view {
    UIView *superView = view.superview;
    NSPredicate *p = [NSPredicate predicateWithFormat:@"firstItem == %@ OR secondItem == %@", view, view];
    NSArray *constraints = [superView.constraints filteredArrayUsingPredicate:p];
    [superView removeConstraints:constraints];
    [view removeConstraints:view.constraints];
}

- (CGRect)frameForViewController:(UIViewController *)viewController withNavigationBarHidden:(BOOL)navigationBarHidden {
    CGRect fullFrame = self.commonContainerView.bounds;
    
    CGRect partFrame = fullFrame;
    CGSize navigationBarSize = [self sizeForNavigationBarInViewController:viewController];
    partFrame.origin.y = navigationBarSize.height;
    partFrame.size.height = CGRectGetHeight(fullFrame) - navigationBarSize.height;
    
    if (!navigationBarHidden) {
        if (viewController.translucentNavigationBar) {
            return fullFrame;
        } else {
            return partFrame;
        }
    } else {
        return fullFrame;
    }
}

- (CGSize)sizeForNavigationBarInViewController:(UIViewController *)viewController {
    return [self sizeForNavigationBar:self.navigationBar];
}

- (CGRect)frameForNavigationBarHidden:(BOOL)hidden inViewController:(UIViewController *)viewController {

    CGFloat topPadding = 0;
    if (@available(iOS 11.0, *)) {
        UIWindow *window = UIApplication.sharedApplication.keyWindow;
        topPadding = window.safeAreaInsets.top;
    }
    
    CGSize size = [self sizeForNavigationBarInViewController:viewController];
    CGPoint origin = CGPointMake(0, 0);
    if (hidden) {
        origin.y = -size.height - topPadding;
    }
    return CGRectMake(origin.x, origin.y, size.width, size.height);
}

- (id<UIViewControllerInteractiveTransitioning>)interactionControllerForAnimator:(id<UIViewControllerAnimatedTransitioning>)animationController animatorIsDefault:(BOOL)animatorIsDefault {
    if (self.privateInteractiveTransition.recognizer.state == UIGestureRecognizerStateBegan) {
        self.privateInteractiveTransition.animator = animationController;
        return self.privateInteractiveTransition;
    } else if (!animatorIsDefault && [self.delegate respondsToSelector:@selector(navigationController:interactiveControllerForAnimationController:)]) {
        return [self.delegate navigationController:self interactiveControllerForAnimationController:animationController];
    } else {
        return nil;
    }
}

- (void)displayRootViewControllerIfNeeded {
    if (self.rootNeedPresent && self.rootViewController) {
        [self displayContentController:self.rootViewController];
        self.rootNeedPresent = NO;
    }
}

- (void)setupRelationshipsForViewController:(UIViewController *)viewController {
    if ([viewController isKindOfClass:[KATabBarController class]]) {
        KATabBarController *tabBarController = (id)viewController;
        for (KATabBar *tabBar in tabBarController.tabBars) {
            for (UIViewController *childVC in tabBar.viewControllers) {
                if (![childVC isKindOfClass:[KANavigationController class]] && !childVC.ka_navigationController) {
                    childVC.ka_navigationController = self;
                }
            }
        }
    }
    else if (self.ka_tabBarController) {
        if (!viewController.ka_tabBarController) {
            viewController.ka_tabBarController = self.ka_tabBarController;
        }
    }
}

#pragma mark - Setters/Getters

- (UIGestureRecognizer *)interactivePopGestureRecognizer {
    return self.privateInteractiveTransition.recognizer;
}

- (void)setViewControllers:(NSArray *)viewControllers {
    [self setViewControllers:viewControllers animated:NO];
}

- (void)setViewControllers:(NSArray *)viewControllers animated:(BOOL)animated {
    NSInteger count = viewControllers.count;
    NSAssert(count > 0, @"You can't set empty array for viewControllers property");
    [viewControllers enumerateObjectsUsingBlock:^(UIViewController *vc, NSUInteger idx, BOOL *stop) {
        NSAssert([vc isKindOfClass:[UIViewController class]], @"Check array of viewControllers you trying to set. All viewControllers should be subclass of UIViewController.");
    }];
    
    if (!self.topViewController) {
        if (count == 1) {
            [self updateRootViewController:viewControllers.firstObject];
            return;
        }
    }
    
    UIViewController *topViewController = viewControllers.lastObject;
    __weak typeof(self) weakSelf = self;
    [self pushViewController:topViewController animated:animated completion:^{
        weakSelf.internalViewControllers = viewControllers;
        weakSelf.rootViewController = weakSelf.internalViewControllers.firstObject;
    }];
}

- (NSArray *)viewControllers {
    [self loadRootViewControllerIfNeeded];
    return self.internalViewControllers;
}

- (void)setRootViewController:(UIViewController *)rootViewController {
    self.internalRootViewController = rootViewController;
}

- (UIViewController *)rootViewController {
    [self loadRootViewControllerIfNeeded];
    return self.internalRootViewController;
}

- (void)loadRootViewControllerIfNeeded {
    if ((!self.internalViewControllers || self.internalViewControllers.count == 0) && !self.rootNeedPresent) {
        NSString *segueIdentifier = [NSString stringWithFormat:@"%@", customNavigationSegueIdentifierBaseString];
        if ([self canPerformSegueWithIdentifier:segueIdentifier]) {
            [self performSegueWithIdentifier:segueIdentifier sender:self];
        } else {
            NSLog(@"[KANavigationController root view controller error] If you want to set root view controller from storyboard set %@ as identifier.", segueIdentifier);
        }
        self.rootNeedPresent = YES;
    }
}

#pragma mark - UIViewController methods

- (NSString *)title{
    return self.rootViewController.title;
}

- (UIImage *)tabBarImageForState:(UIControlState)state {
    return [self.rootViewController tabBarImageForState:state];
}

#pragma mark - Push

- (void)pushViewController:(UIViewController *)vc animated:(BOOL)animated completion:(void (^)())completion {
    [self cycleFromViewController:self.topViewController toViewController:vc transitionType:KANavigationControllerTransitionTypePush animated:animated completion:completion];
}

#pragma mark - Pop

- (void)popViewControllerAnimated:(BOOL)animated completion:(void (^)())completion {
    NSUInteger count = self.internalViewControllers.count;
    if (count > 1) {
        UIViewController *pvc = self.internalViewControllers[count-2];
        __weak typeof(self) weakSelf = self;
        __block UIViewController *topViewController = weakSelf.topViewController;
        [self cycleFromViewController:self.topViewController toViewController:pvc transitionType:KANavigationControllerTransitionTypePop animated:animated completion:^{
            NSMutableArray *ma = [NSMutableArray arrayWithArray:weakSelf.internalViewControllers];
            [ma removeObject:topViewController];
            weakSelf.internalViewControllers = [NSArray arrayWithArray:ma];
            if (completion)
                completion();
        }];
    }
}

- (void)popToRootViewControllerAnimated:(BOOL)animated completion:(void (^)())completion {
    NSUInteger count = self.internalViewControllers.count;
    if (count > 1) {
        __weak typeof(self) weakSelf = self;
        [self cycleFromViewController:self.topViewController toViewController:self.rootViewController transitionType:KANavigationControllerTransitionTypePop animated:animated completion:^{
            weakSelf.internalViewControllers = @[weakSelf.rootViewController];
            if (completion)
                completion();
        }];
    }
}

- (void)popToViewController:(UIViewController*)vc animated:(BOOL)animated completion:(void (^)())completion {
    NSUInteger count = self.internalViewControllers.count;
    if (count > 1) {
        __weak typeof(self) weakSelf = self;
        [self cycleFromViewController:self.topViewController toViewController:vc transitionType:KANavigationControllerTransitionTypePop animated:animated completion:^{
            NSMutableArray *controllers = [NSMutableArray array];
            for (UIViewController *controller in weakSelf.internalViewControllers) {
                [controllers addObject:controller];
                if ([controller isKindOfClass:[vc class]]) {
                    break;
                }
            }
            weakSelf.internalViewControllers = [NSArray arrayWithArray:controllers];
            if (completion)
                completion();
        }];
    }
}

- (void)adjustScrollViewInsetsIfNeededForViewController:(UIViewController *)viewController {
    if (viewController.ka_automaticallyAdjustsScrollViewInsets) {
        NSPredicate *p = [NSPredicate predicateWithFormat:@"self isKindOfClass: %@", [UIScrollView class]];
        NSArray *scrollViews = [viewController.view.subviews filteredArrayUsingPredicate:p];
        for (UIScrollView *sv in scrollViews) {
            UIEdgeInsets insets = sv.contentInset;
            CGPoint offset = sv.contentOffset;
            if (!self.navigationBarShouldBeHidden) {
                CGFloat navigationHeight = [self sizeForNavigationBarInViewController:viewController].height;
                insets.top += navigationHeight;
                offset.y -= navigationHeight;
            }
            [sv setContentInset:insets];
            [sv setScrollIndicatorInsets:insets];
            [sv setContentOffset:offset];
        }
    }
}

#pragma mark - Content Controller Handling

- (void)updateRootViewController:(UIViewController *)vc {
    self.internalViewControllers = @[vc];
    self.topViewController = vc;
    self.rootViewController = vc;
    vc.ka_navigationController = self;
}

- (void)displayContentController:(UIViewController *)contentController {
    [self addChildViewController:contentController];
    UIView *view = contentController.view;
    [self.commonContainerView addSubview:view];
    [view setFrame:[self frameForViewController:contentController withNavigationBarHidden:self.navigationBarShouldBeHidden]];
    [self adjustScrollViewInsetsIfNeededForViewController:contentController];
    [contentController didMoveToParentViewController:self];
    
    [self.navigationBar needHideBackButtonWithAnimationDuration:0 fromViewController:nil toViewController:contentController];
    [self.navigationBar needUpdateWithTransitionType:KANavigationControllerTransitionTypePush withAnimationDuration:0 fromViewController:nil toViewController:contentController];
    
    [self setupRelationshipsForViewController:contentController];
}

- (void)hideContentController:(UIViewController *)content {
    [content beginAppearanceTransition:NO animated:NO];
    [content willMoveToParentViewController:nil];
    [content.view removeFromSuperview];
    [content removeFromParentViewController];
    [content endAppearanceTransition];
}

- (void)cycleFromViewController:(UIViewController*)ovc toViewController:(UIViewController*)nvc transitionType:(KANavigationControllerTransitionType)type animated:(BOOL)animated completion:(void (^)())completion{
    
    if(self.inTransition)
        return;
    
    self.inTransition = YES;
    [self.view setUserInteractionEnabled:NO];
    
    if (type == KANavigationControllerTransitionTypePush) {
        nvc.ka_navigationController = self;
    }
    
    self.lastTransitionType = type;
    self.lastTransitionTypeUsed = NO;
    
    self.checkingNavBarHidden = YES;
    UIView *nview = nvc.view;
    UIView *oview = ovc.view;
    BOOL navBarInitiallyHidden = self.navigationBar.hidden;
    [ovc beginAppearanceTransition:NO animated:animated];
    [nvc beginAppearanceTransition:YES animated:animated];
    BOOL navBarShouldBeHiddenOnAppearance = navBarInitiallyHidden || self.navigationBarShouldBeHidden;
    [self.commonContainerView bringSubviewToFront:self.navigationBar];
    
    if (type == KANavigationControllerTransitionTypePush) {
        [self adjustScrollViewInsetsIfNeededForViewController:nvc];
        
        NSMutableArray *ma = [NSMutableArray arrayWithArray:self.internalViewControllers];
        [ma addObject:nvc];
        self.internalViewControllers = [NSArray arrayWithArray:ma];
        
        [self setupRelationshipsForViewController:nvc];
    }
    
    id<UIViewControllerAnimatedTransitioning> animator = nil;
    if ([self.delegate respondsToSelector:@selector(navigationController:animationControllerForTransition:fromViewController:toViewController:)])
        animator = [self.delegate navigationController:self animationControllerForTransition:type fromViewController:ovc toViewController:nvc];
    if(!animator)
        animator = (id)[[KANavigationControllerPrivateAnimatedTransition alloc] init];
    
    __weak typeof(self) weakSelf = self;
    KAPrivateTransitionContext *context = [[KAPrivateTransitionContext alloc] initWithFromViewController:ovc toViewController:nvc navigationController:self transitionType:type navigationBarInitiallyHidden:navBarInitiallyHidden navigationBarShouldBeHidden:self.navigationBarShouldBeHidden];
    [context setAnimated:animated];
    [context setWillCancelInteractiveTransitionBlock:^{
        [nview setHidden:YES];
        [weakSelf.navigationBar needUpdateWithTransitionType:KANavigationControllerTransitionTypePush withAnimationDuration:0 fromViewController:nvc toViewController:ovc];
        if (weakSelf.internalViewControllers.count > 1) {
            [weakSelf.navigationBar needShowBackButtonWithAnimationDuration:0 fromViewController:nvc toViewController:ovc];
        } else {
            [weakSelf.navigationBar needHideBackButtonWithAnimationDuration:0 fromViewController:nvc toViewController:ovc];
        }
    }];
    [context setCompletionBlock:^(BOOL finished) {
        if (finished) {
            if (type == KANavigationControllerTransitionTypePop){
                CGRect nbfFrame = weakSelf.navigationBar.frame;
                if (weakSelf.navigationBarShouldBeHidden) {
                    nbfFrame.origin.x = 0;
                    nbfFrame.origin.y = -CGRectGetHeight(nbfFrame);
                    [weakSelf.navigationBar setFrame:nbfFrame];
                    [weakSelf.navigationBar setHidden:YES];
                }
            }
            
            [ovc willMoveToParentViewController:nil];
            [oview removeFromSuperview];
            [ovc removeFromParentViewController];
            [ovc endAppearanceTransition];
            [nvc didMoveToParentViewController:weakSelf];
            [weakSelf addChildViewController:nvc];
            weakSelf.topViewController = nvc;
            [nvc endAppearanceTransition];
            [weakSelf.view setUserInteractionEnabled:YES];
            
            weakSelf.checkingNavBarHidden = NO;
            
            [weakSelf.navigationBar setFrame:[weakSelf frameForNavigationBarHidden:navBarShouldBeHiddenOnAppearance inViewController:nvc]];
            [weakSelf setNavigationBarHidden:weakSelf.navigationBarShouldBeHidden animated:weakSelf.navigationBarShouldBeHiddenAnimated];
            
            if (completion)
                completion();
        } else {
            [weakSelf.commonContainerView sendSubviewToBack:nview];
            [nvc beginAppearanceTransition:NO animated:animated];
            [nvc endAppearanceTransition];
            [ovc beginAppearanceTransition:YES animated:animated];
            [ovc endAppearanceTransition];
            if ([animator respondsToSelector:@selector(animationEnded:)]) {
                [animator animationEnded:finished];
            }
            weakSelf.checkingNavBarHidden = NO;
            weakSelf.navigationBarShouldBeHidden = navBarInitiallyHidden;
            [weakSelf.view setUserInteractionEnabled:YES];
            dispatch_async(dispatch_get_main_queue(), ^{
                [nview removeFromSuperview];
                [nview setHidden:NO];
            });
        }
        self.inTransition = NO;
    }];
    
    id<UIViewControllerInteractiveTransitioning> interactionController = [self interactionControllerForAnimator:animator animatorIsDefault:[animator isKindOfClass:[KANavigationControllerPrivateAnimatedTransition class]]];
    
    [context setInteractive:(interactionController != nil)];
    
    if ([context isInteractive]) {
        [interactionController startInteractiveTransition:context];
    } else {
        [animator animateTransition:context];
    }
}

#pragma mark - Forwarding Appearance Methods

- (BOOL)shouldAutomaticallyForwardAppearanceMethods {
    return NO;
}

#pragma mark - Navigation Bar

- (KANavigationBar *)requestNavigationBar {
    if ([self.delegate respondsToSelector:@selector(navigationBarForNavigationController:)])
        return [self.delegate navigationBarForNavigationController:self];
    return [[KANavigationBar alloc] init];
}

- (void)setNavigationBarHidden:(BOOL)hidden animated:(BOOL)animated {
    if (self.checkingNavBarHidden) {
        [self setNavigationBarShouldBeHidden:hidden];
        [self setNavigationBarShouldBeHiddenAnimated:animated];
        return;
    }
    
    [self.view setUserInteractionEnabled:NO];
    
    CGRect nviewNewFrame = [self frameForViewController:self.topViewController withNavigationBarHidden:hidden];
    CGRect navBarNewFrame = [self frameForNavigationBarHidden:hidden inViewController:self.topViewController];
    
    if (animated) {
        [UIView animateWithDuration:.2f delay:.0f options:UIViewAnimationOptionCurveEaseInOut|UIViewAnimationOptionBeginFromCurrentState animations:^{
            [self.navigationBar setFrame:navBarNewFrame];
            [self.topViewController.view setFrame:nviewNewFrame];
            [self.topViewController.view layoutIfNeeded];
        } completion:^(BOOL finished) {
            [self.navigationBar setHidden:hidden];
            [self.view setUserInteractionEnabled:YES];
        }];
    } else {
        [self.navigationBar setFrame:navBarNewFrame];
        [self.topViewController.view setFrame:nviewNewFrame];
        [self.navigationBar setHidden:hidden];
        [self.view setUserInteractionEnabled:YES];
        [self.view layoutIfNeeded];
    }
}

- (void)navigationBarBackButtonPressed:(KANavigationBar *)navigationBar {
    [self popViewControllerAnimated:YES completion:nil];
}

- (CGSize)sizeForNavigationBar:(KANavigationBar *)navigationBar {
    double width = CGRectGetWidth([UIScreen mainScreen].bounds);
    if ([self.delegate respondsToSelector:@selector(navigationController:sizeForNavigationBar:)])
        return [self.delegate navigationController:self sizeForNavigationBar:navigationBar];
    return CGSizeMake(width, 64.f);
}

#pragma mark - Private

- (void)ka_tabBarControllerDidStartTransition:(KATabBarController *)ka_tabBarController from:(UIViewController *)fromVC to:(UIViewController *)toVC {
    self.checkingNavBarHidden = YES;
    self.navigationBarInitiallyHidden = self.navigationBar.hidden;
    self.navigationBarShouldBeHiddenOnAppearance = self.navigationBarInitiallyHidden || self.navigationBarShouldBeHidden;
}

- (void)ka_tabBarControllerDidFinishTransition:(KATabBarController *)ka_tabBarController from:(UIViewController *)fromVC to:(UIViewController *)toVC {
    self.checkingNavBarHidden = NO;
    [self.navigationBar setFrame:[self frameForNavigationBarHidden:self.navigationBarShouldBeHiddenOnAppearance inViewController:toVC]];
    [self setNavigationBarHidden:self.navigationBarShouldBeHidden animated:self.navigationBarShouldBeHiddenAnimated];
}

- (void)ka_tabBarControllerDidCancelTransition:(KATabBarController *)ka_tabBarController from:(UIViewController *)fromVC to:(UIViewController *)toVC {
    self.checkingNavBarHidden = NO;
    self.navigationBarShouldBeHidden = self.navigationBarInitiallyHidden;
}

@end


#pragma mark - UIViewControllerCategory

@implementation UIViewController (NavigationController)

- (void)setKa_navigationController:(KANavigationController *)ka_navigationController {
    objc_setAssociatedObject(self, @selector(setKa_navigationController:), ka_navigationController, OBJC_ASSOCIATION_ASSIGN);
}

- (KANavigationController *)ka_navigationController {
    return objc_getAssociatedObject(self, @selector(setKa_navigationController:));
}

- (void)setKa_leftNavigationItem:(UIView *)ka_leftNavigationItem {
    objc_setAssociatedObject(self, @selector(setKa_leftNavigationItem:), ka_leftNavigationItem, OBJC_ASSOCIATION_RETAIN_NONATOMIC);
}

- (UIView *)ka_leftNavigationItem {
    return objc_getAssociatedObject(self, @selector(setKa_leftNavigationItem:));
}

- (void)setKa_rightNavigationItem:(UIView *)ka_rightNavigationItem {
    objc_setAssociatedObject(self, @selector(setKa_rightNavigationItem:), ka_rightNavigationItem, OBJC_ASSOCIATION_RETAIN_NONATOMIC);
}

- (UIView *)ka_rightNavigationItem {
    return objc_getAssociatedObject(self, @selector(setKa_rightNavigationItem:));
}

- (void)setKa_automaticallyAdjustsScrollViewInsets:(BOOL)ka_automaticallyAdjustsScrollViewInsets {
    objc_setAssociatedObject(self, @selector(setKa_automaticallyAdjustsScrollViewInsets:), @(ka_automaticallyAdjustsScrollViewInsets), OBJC_ASSOCIATION_RETAIN_NONATOMIC);
}

- (BOOL)ka_automaticallyAdjustsScrollViewInsets {
    return [objc_getAssociatedObject(self, @selector(setKa_automaticallyAdjustsScrollViewInsets:)) boolValue];
}

- (void)setTranslucentNavigationBar:(BOOL)translucentNavigationBar {
    objc_setAssociatedObject(self, @selector(setTranslucentNavigationBar:), @(translucentNavigationBar), OBJC_ASSOCIATION_RETAIN_NONATOMIC);
}

- (BOOL)translucentNavigationBar {
    return [objc_getAssociatedObject(self, @selector(setTranslucentNavigationBar:)) boolValue];
}

@end


#pragma mark - CUSTOM SEGUE

NSString* const customNavigationSegueIdentifierBaseString = @"KAROOT";
@implementation CustomNavigationRootViewControllerSegue

- (void)perform {
    KANavigationController *svc = self.sourceViewController;
    UIViewController *dvc = self.destinationViewController;
    [svc setViewControllers:@[dvc]];
}

@end


@implementation CustomNavigationPushViewControllerSegue

- (void)perform {
    UIViewController *svc = self.sourceViewController;
    UIViewController *dvc = self.destinationViewController;
    KANavigationController *nav = svc.ka_navigationController;
    [nav pushViewController:dvc animated:YES completion:nil];
}

@end
