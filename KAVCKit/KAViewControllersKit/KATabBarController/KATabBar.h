//
//  KATabBar.h
//  Collaborate
//
//  Created by Anton on 14.01.15.
//  Copyright (c) 2015 Rosberry. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "KATabBarButtonContainerView.h"

/*!
 * @typedef KATabBarOrientation
 * @brief Orientation for tab bar
 * @constant KATabBarOrientationHorizontal horizontal orientation
 * @constant KATabBarOrientationVertical vertical orientation
 */
typedef NS_ENUM(NSUInteger, KATabBarOrientation){
    KATabBarOrientationHorizontal,
    KATabBarOrientationVertical
};

@class KATabBar;
@protocol KATabBarDelegate <NSObject>

@required
/*!
 * @discussion Tells the delegate that transition between view controllers should start.
 * @param force force the animation or not.
 * @param change Perform selection change for tab bar buttons in this block
 */
- (void)tabBar:(KATabBar *)tabBar shouldCycleToViewController:(UIViewController *)toViewController animated:(BOOL)animated force:(BOOL)force changeSelections:(void(^)())change completion:(void(^)(BOOL finished))completion;

@optional
///Change tab bar intial constraints if you want so.
- (NSArray *)tabBarConstraints:(KATabBar *)tabBar;
///Ask the delegate whether view controller should be selected or not.
- (BOOL)tabBar:(KATabBar *)tabBar shouldSelectViewController:(UIViewController *)vc;
///Tells the delegate that view controller has been selected.
- (void)tabBar:(KATabBar *)tabBar didSelectViewController:(UIViewController *)vc;
///Asks the delegate for button for view controller.
- (UIButton *)tabBar:(KATabBar *)tabBar buttonForViewController:(UIViewController *)vc atIndex:(NSUInteger)index;
///Ask the delegate width for delimeter between buttons.
- (UIColor *)tabBarButtonsDelimeterColor:(KATabBar *)tabBar;
///Ask the delegate width for delimeter between tab bar and other views.
- (UIColor *)tabBarDelimeterColor:(KATabBar *)tabBar;
///Change tab bar orientation here
- (KATabBarOrientation)tabBarOrientation:(KATabBar *)tabBar;
///Ask the delegate which color should be used for delimeter between buttons.
- (double)tabBarDelimeterWidth:(KATabBar *)tabBar;

///Tells the delegate that the user selected active view controller in the tab bar.
- (void)tabBarDidSelectCurrentViewController:(KATabBar *)tabBar;

@end


@interface KATabBar : UIView

///Container view for all buttons in tab bar
@property (nonatomic, strong, readonly) UIView *buttonsContainerView;
///Array of view controllers
@property (nonatomic, strong) NSArray *viewControllers;
///Current selected view controller
@property (nonatomic, weak) UIViewController *selectedViewController;
///Index of current seleted view controller
@property (nonatomic, assign) NSUInteger selectedIndex;
///Index of previous selected view controller
@property (nonatomic, assign, readonly) NSUInteger previousSelectedIndex;
///Show if previousSelectedIndex property used
@property (nonatomic, assign) BOOL previousSelectionUsed;
///Index of tab bar in Tab Bar Controller
@property (nonatomic, assign) NSUInteger index;
///Delegate that conform to KATabBarDelegate protocol
@property (nonatomic, weak) id<KATabBarDelegate> delegate;
///Orientation of tab bar
@property (nonatomic, assign, readonly) KATabBarOrientation orientation;

///Use this method if you need to recreate tab bar buttons
- (void)updateTabBarButtons;
///Use this method if you need to reset selection
- (void)resetSelection;
///You can set selected view controller indirectly by using this method.
- (void)setSelectedViewControllerWithClass:(Class)class_;

///Get button for index
- (UIButton *)buttonAtIndex:(NSUInteger)index;
///Use this method to change selected view controller
- (void)setSelectedIndex:(NSUInteger)selectedIndex animated:(BOOL)animated;

@end
