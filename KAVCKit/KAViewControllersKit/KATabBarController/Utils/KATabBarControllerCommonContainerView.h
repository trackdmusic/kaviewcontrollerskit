//
//  KATabBarControllerCommonContainerView.h
//  KAVCKit
//
//  Created by Anton Kovalev on 05.04.16.
//  Copyright © 2016 Rosberry. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface KATabBarControllerCommonContainerView : UIView

@end
