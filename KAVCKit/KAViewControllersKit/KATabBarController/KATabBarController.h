//
//  CustomTabBarViewController.h
//  Q-Less vendor
//
//  Created by Anton on 08.10.14.
//  Copyright (c) 2014 Rosberry. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "KATabBar.h"
@class KATabBarController;
@class KANavigationController;
@class KATabBarControllerControllersContainerView;
@class KATabBarControllerCommonContainerView;

@protocol KATabBarControllerDelegate <NSObject>

/*!
 * @discussion Set number of tabBars you want here.
 */
- (NSUInteger)ka_tabBarControllerNumberOfTabBars:(KATabBarController *)tabBarController;
/*!
 * @discussion Set sprecific tabBar for index.
 * @return Instance of KATabBar or subclass.
 */
- (KATabBar *)tabBarAtIndex:(NSUInteger)index;
/*!
 * @discussion Provide button for each view controller in tab bar.
 * @param index Index of view controller in tab bar.
 * @return Instance if UIButton.
 */
- (UIButton *)ka_tabBarController:(KATabBarController *)tabBarController buttonForViewController:(UIViewController *)vc inTabBar:(KATabBar *)tabBar atIndex:(NSUInteger)index;
/*!
 * @discussion Tab bar can be horizontal or vertical.
 * @return Orientation described in "KATabBarOrientation"
 */
- (KATabBarOrientation)ka_tabBarController:(KATabBarController *)tabBarController orientationForTabBar:(KATabBar *)tabBar;

/*!
 * @discussion Set specific size and place for each tabbar
 */
- (CGRect)ka_tabBarController:(KATabBarController *)tabBarController frameForTabBar:(KATabBar *)tabBar;
/*!
 * @discussion Set specific size and place for controllers container view
 */
- (CGRect)ka_tabBarController:(KATabBarController *)tabBarController frameForControllersContainerView:(KATabBarControllerControllersContainerView *)controllersContainerView;

@optional
/*!
 * @discussion Use this delegate method to return non-interactive transition animator which is used to make custom animation for transition betwen controllers.
 * @return The animator object responsible for managing the transition animations, or nil if you want to use the standard navigation controller transitions.
 */
- (id<UIViewControllerAnimatedTransitioning>)tabBarController:(KATabBarController *)tabBarController animationControllerForTransitionFromViewController:(UIViewController *)fromVC toViewController:(UIViewController *)toVC;
/*!
 * @discussion Use this delegate method to return interactive controller for transition animator which is used to make custom animation for transition betwen controllers.
 * @param animationController The noninteractive animator object provided by the delegate’s tabBarController:animationControllerForTransitionFromViewController:toViewController: method.
 * @return The animator object responsible for managing the transition animations, or nil if you want to use the standard navigation controller transitions.
 */
- (id<UIViewControllerInteractiveTransitioning>)tabBarController:(KATabBarController *)tabBarController interactiveControllerForAnimationController:(id<UIViewControllerAnimatedTransitioning>)animationController;

/*!
 * @discussion You can set view controllers by using this method.
 * @return Array of view controllers;
 */
- (NSArray *)ka_tabBarController:(KATabBarController *)tabBarController viewControllersForTabBar:(KATabBar *)tabBar;

/*!
 * @discussion Asks the delegate whether the specified view controller should be made active.
 * @return YES if the view controller’s tab should be selected or NO if the current tab should remain active.
 */
- (BOOL)ka_tabBarController:(KATabBarController *)tabBarController shouldSelectViewController:(UIViewController *)viewController inTabBar:(KATabBar *)tabBar;
///Tells the delegate that the user selected view controller in the tab bar.
- (void)ka_tabBarController:(KATabBarController *)tabBarController didSelectViewController:(UIViewController *)viewController inTabBar:(KATabBar *)tabBar;
///Tells the delegate that the user selected active view controller in the tab bar.
- (void)ka_tabBarController:(KATabBarController *)tabBarController didSelectCurrentViewControllerInTabBar:(KATabBar *)tabBar;

/*!
 * @discussion If active view controller has subview kind of scroll on first level subviews and user select active tab then this scroll view will scroll to top. This method tells delegate about it.
 */
- (void)ka_tabBarController:(KATabBarController *)tabBarController willScrollScrollView:(UIScrollView *)scrollView toZeroPoint:(CGPoint)zeroPoint inViewController:(UIViewController *)vc;

/*!
 * @discussion Zero state is specific state, when no tabs is selected but some view controller presented.
 *
 * Tells the delegate that tab bar controller will show view controller in zero state.
 */
- (void)ka_tabBarController:(KATabBarController *)tabBarController willShowViewControllerInZeroState:(UIViewController *)viewController;
/*!
 * @discussion Zero state is specific state, when no tabs is selected but some view controller presented.
 *
 * Tells the delegate that tab bar controller did show view controller in zero state.
 */
- (void)ka_tabBarController:(KATabBarController *)tabBarController didShowViewControllerInZeroState:(UIViewController *)viewController;
/*!
 * @discussion Zero state is specific state, when no tabs is selected but some view controller presented.
 *
 * Tells the delegate that tab bar controller will hide view controller in zero state.
 */
- (void)ka_tabBarController:(KATabBarController *)tabBarController willHideViewControllerInZeroState:(UIViewController *)viewController;
/*!
 * @discussion Zero state is specific state, when no tabs is selected but some view controller presented.
 *
 * Tells the delegate that tab bar controller did hide view controller in zero state.
 */
- (void)ka_tabBarController:(KATabBarController *)tabBarController didHideViewControllerInZeroState:(UIViewController *)viewController;

/*!
 * @discussion Use this method if you want to add additional views to tabBarController's common container view.
 */
- (void)ka_tabBarController:(KATabBarController *)tabBarController performAdditionalConfigurationsForCommonContainerView:(KATabBarControllerCommonContainerView *)containerView;

/*!
 * @discussion Use this method if you want to change initial tabBar constraints.
 */
- (NSArray *)ka_tabBarController:(KATabBarController *)tabBarController constraintsForTabBar:(KATabBar *)tabBar;

///Ask the delegate which color should be used for delimeter between buttons.
- (UIColor *)ka_tabBarController:(KATabBarController *)tabBarController delimeterColorForTabBar:(KATabBar *)tabBar;
///Ask the delegate width for delimeter between buttons.
- (double)ka_tabBarController:(KATabBarController *)tabBarController delimeterWidthForTabBar:(KATabBar *)tabBar;

@end


@interface KATabBarController : UIViewController

///View which contains active view controller's view
@property (nonatomic, strong, readonly) KATabBarControllerControllersContainerView *viewControllersContainerView;
///One of view controllers in this tab bar present on the screen
@property (nonatomic, weak, readonly) KATabBar *activeTabBar;
///Array of all tab bars
@property (nonatomic, strong, readonly) NSArray *tabBars;
///Delegate that conform to KATabBarControllerDelegate protocol
@property (nonatomic, weak) id<KATabBarControllerDelegate> delegate;

///Use this property if you want to switch tabs and transition methods (like viewWillAppear:) should not be called. Do not forget to turn it off after switching tabs.
@property (nonatomic, assign) BOOL shouldNotPerformTransitions;
/*!
 * @discussion Zero state is specific state, when no tabs is selected but some view controller presented.
 *
 * Check if tab bar controller present some view controller in zero state.
 */
@property (nonatomic, assign, readonly) BOOL isInZeroState;

/*!
 * @discussion Zero state is specific state, when no tabs is selected but some view controller presented.
 *
 * Use this method if you want to present some view controller in zero state.
 */
- (void)showViewControllerInZeroState:(UIViewController *)vc animated:(BOOL)animated;

@end


@interface UIViewController (CustomTabBarController)

/*!
 * @discussion The nearest ancestor in the view controller hierarchy that is a tab bar controller. (read-only)
 */
@property (nonatomic, weak) KATabBarController *ka_tabBarController;
///Button which represent view controller in tab bar.
@property (nonatomic, weak) UIButton *tabBarButton;
///Override this method for more comfortable work with delegate methods 
- (UIImage *)tabBarImageForState:(UIControlState)state;

@end


extern NSString * const customSegueIdentifierBaseString;
@interface CustomTabBarSetViewControllerSegue : UIStoryboardSegue
@end
