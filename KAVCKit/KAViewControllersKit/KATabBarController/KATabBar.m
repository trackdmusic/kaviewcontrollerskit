//
//  KATabBar.m
//  Collaborate
//
//  Created by Anton on 14.01.15.
//  Copyright (c) 2015 Rosberry. All rights reserved.
//

#define kButtonTagOffset        1245
#define kDelimeterViewTag       1205

#import "KATabBar.h"

@interface KATabBar ()

@property (nonatomic, strong) NSArray *buttons;

@end

@implementation KATabBar

- (void)dealloc{
    _viewControllers = nil;
}

#pragma mark - Tab Bar Buttons

- (void)createTabBarButtons{
    _orientation = [self tabBarOrientation];
    BOOL horVer = (_orientation == KATabBarOrientationHorizontal);
    NSMutableString *hConstraintString = [[NSString stringWithFormat:@"%@:|", horVer ? @"H" : @"V"] mutableCopy];
    NSMutableArray *vConstraintsStrings = [@[] mutableCopy];
    NSMutableDictionary *views = [@{} mutableCopy];
    NSInteger k = 0;
    double delimeterWidth = [self delimiterWidth];
    NSMutableArray *ma = [@[] mutableCopy];
    for (UIViewController *vc in self.viewControllers) {
        NSAssert([vc isKindOfClass:[UIViewController class]], @"You can't use anything but UIViewController subclass");
        
        if (!vc.title)
            [vc setTitle:[NSString stringWithFormat:@"vc%li", (long)k]];
        UIButton *btn = [self buttonForViewController:vc];
        [btn setTag:kButtonTagOffset + k];
        [btn addTarget:self action:@selector(tabBarButtonPressed:) forControlEvents:UIControlEventTouchUpInside];
        [btn setExclusiveTouch:YES];
        [btn setTranslatesAutoresizingMaskIntoConstraints:NO];
        KATabBarButtonContainerView *container = [[KATabBarButtonContainerView alloc] init];
        [container setTranslatesAutoresizingMaskIntoConstraints:NO];
        [container addSubview:btn];
        if (btn) {
            [ma addObject:btn];
            NSArray *bch = [NSLayoutConstraint constraintsWithVisualFormat:@"H:|[btn]|" options:0 metrics:nil views:@{@"btn":btn}];
            NSArray *bcv = [NSLayoutConstraint constraintsWithVisualFormat:@"V:|[btn]|" options:0 metrics:nil views:@{@"btn":btn}];
            [container addConstraints:[bch arrayByAddingObjectsFromArray:bcv]];
        }
        
        [self.buttonsContainerView addSubview:container];
        
        NSString *btnKey = [NSString stringWithFormat:@"btn%li", (long)k];
        NSString *btnPrevKey = (k > 0) ? [NSString stringWithFormat:@"(==btn%li)", (long)k-1] : @"";
        [views setObject:container forKey:btnKey];
        [vConstraintsStrings addObject:[NSString stringWithFormat:@"%@:|[%@]|", horVer ? @"V" : @"H", btnKey]];
        
        [hConstraintString appendFormat:@"[%@%@]", btnKey, btnPrevKey];
        if ([vc isEqual:[self.viewControllers lastObject]])
            [hConstraintString appendString:@"|"];
        else {
            UIView *delimiterView = [[UIView alloc] init];
            [delimiterView setTranslatesAutoresizingMaskIntoConstraints:NO];
            NSString *dKey = [NSString stringWithFormat:@"delimeter%li", (long)k];
            [views setObject:delimiterView forKey:dKey];
            [delimiterView setBackgroundColor:[self buttonsDelimeterColor]];
            [hConstraintString appendFormat:@"[%@(%li)]", dKey, (long)delimeterWidth];
            [vConstraintsStrings addObject:[NSString stringWithFormat:@"%@:|[%@]|", horVer ? @"V" : @"H", dKey]];
            [self.buttonsContainerView addSubview:delimiterView];
        }
        ++k;
    }
    _buttons = [NSArray arrayWithArray:ma];
    if (k == 0)
        return;
    
    NSArray *constraints = [NSLayoutConstraint constraintsWithVisualFormat:hConstraintString options:0 metrics:nil views:[NSDictionary dictionaryWithDictionary:views]];
    [self.buttonsContainerView addConstraints:constraints];
    
    for (NSString *str in vConstraintsStrings) {
        NSArray *constraints = [NSLayoutConstraint constraintsWithVisualFormat:str options:0 metrics:nil views:[NSDictionary dictionaryWithDictionary:views]];
        [self.buttonsContainerView addConstraints:constraints];
    }
    UIView *delimeterView = [self viewWithTag:kDelimeterViewTag];
    [self bringSubviewToFront:delimeterView];
}

- (void)removeTabBarButtons{
    for (UIView *sv in self.buttonsContainerView.subviews)
        [sv removeFromSuperview];
}

- (void)updateTabBarButtons{
    [self removeTabBarButtons];
    [self createTabBarButtons];
}

#pragma mark - viewControllers

- (void)setViewControllers:(NSArray *)viewControllers{
    _viewControllers = viewControllers;
    _buttonsContainerView = [[UIView alloc] init];
    [_buttonsContainerView setTranslatesAutoresizingMaskIntoConstraints:NO];
    [self addSubview:_buttonsContainerView];
    
    NSDictionary *views = NSDictionaryOfVariableBindings(_buttonsContainerView);
    NSArray *ch = [NSLayoutConstraint constraintsWithVisualFormat:@"H:|[_buttonsContainerView]|" options:0 metrics:nil views:views];
    NSArray *cv = [NSLayoutConstraint constraintsWithVisualFormat:@"V:|[_buttonsContainerView]|" options:0 metrics:nil views:views];
    NSArray *initialConstraints = [ch arrayByAddingObjectsFromArray:cv];
    
    NSArray *constraints = [self tabBarConstraints];
    if (!constraints) {
        constraints = initialConstraints;
    }
    [self addConstraints:constraints];
    [self updateTabBarButtons];
}

#pragma mark - Selected Index

- (void)setSelectedIndex:(NSUInteger)selectedIndex {
    [self setSelectedIndex:selectedIndex animated:NO force:NO];
}

- (void)setSelectedIndex:(NSUInteger)selectedIndex animated:(BOOL)animated {
    [self setSelectedIndex:selectedIndex animated:animated force:YES];
}

- (void)setSelectedIndex:(NSUInteger)selectedIndex animated:(BOOL)animated force:(BOOL)force {
    if (selectedIndex < self.viewControllers.count) {
        UIViewController *vc = self.viewControllers[selectedIndex];
        if ([self.delegate respondsToSelector:@selector(tabBar:shouldSelectViewController:)])
            if (![self.delegate tabBar:self shouldSelectViewController:vc])
                return;
        
        
        [self.delegate tabBar:self shouldCycleToViewController:vc animated:animated force:force changeSelections:^{
            _previousSelectedIndex = (_selectedIndex != NSNotFound) ? _selectedIndex : _previousSelectedIndex;
            _previousSelectionUsed = NO;
            _selectedIndex = selectedIndex;
            _selectedViewController = vc;
        } completion:^(BOOL finished) {
            if (finished) {
                if ([self.delegate respondsToSelector:@selector(tabBar:didSelectViewController:)])
                    [self.delegate tabBar:self didSelectViewController:vc];
                
                for (UIButton *b in _buttons) {
                    NSInteger bIndex = b.tag - kButtonTagOffset;
                    if (bIndex == self.selectedIndex)
                        [b setSelected:YES];
                    else
                        [b setSelected:NO];
                }
            }
        }];
    }
}

#pragma mark - Selected ViewController

- (void)setSelectedViewController:(UIViewController *)selectedViewController{
    if ([selectedViewController isEqual:_selectedViewController])
        return;
    if (!selectedViewController) {
        _selectedViewController = nil;
        _previousSelectedIndex = _selectedIndex;
        _previousSelectionUsed = NO;
        _selectedIndex = NSNotFound;
        return;
    }
    NSUInteger index = [self.viewControllers indexOfObject:selectedViewController];
    if (index == NSNotFound) {
        _previousSelectedIndex = (_selectedIndex != NSNotFound) ? _selectedIndex : _previousSelectedIndex;
        _previousSelectionUsed = NO;
        _selectedIndex = index;
        return;
    }
    
    [self setSelectedIndex:index];
}

- (void)setSelectedViewControllerWithClass:(Class)class_{
    NSPredicate *predicate = [NSPredicate predicateWithBlock:^BOOL(id evaluatedObject, NSDictionary *bindings) {
        if ([evaluatedObject respondsToSelector:@selector(rootViewController)]){
            UIViewController *rootViewController = [evaluatedObject rootViewController];
            return [rootViewController isKindOfClass:class_];
        }
        else{
            return [evaluatedObject isKindOfClass:class_];
        }
    }];
    UIViewController *viewController = [[_viewControllers filteredArrayUsingPredicate:predicate] firstObject];
    if (viewController){
        NSInteger index = [_viewControllers indexOfObject:viewController];
        [self setSelectedIndex:index];
    }
}

- (UIButton *)buttonAtIndex:(NSUInteger)index {
    for (UIButton *b in _buttons) {
        NSInteger bIndex = b.tag - kButtonTagOffset;
        if (bIndex == self.selectedIndex)
            return b;
    }
    return nil;
}

#pragma mark - Methods

- (UIButton *)buttonForViewController:(UIViewController *)vc{
    if ([self.delegate respondsToSelector:@selector(tabBar:buttonForViewController:atIndex:)])
        return [self.delegate tabBar:self buttonForViewController:vc atIndex:[_viewControllers indexOfObject:vc]];
    
    UIButton *btn = [UIButton buttonWithType:UIButtonTypeCustom];
    [btn setBackgroundColor:[UIColor clearColor]];
    [btn setTitle:vc.title forState:UIControlStateNormal];
    [btn setTitleColor:[UIColor colorWithRed:0.82 green:0.42 blue:0.33 alpha:1] forState:UIControlStateNormal];
    [btn setTitleColor:[UIColor colorWithRed:0.25 green:0.84 blue:0.46 alpha:1] forState:UIControlStateSelected];
    [btn setTitleColor:[UIColor colorWithRed:0.25 green:0.84 blue:0.46 alpha:1] forState:UIControlStateSelected|UIControlStateHighlighted];
    return btn;
}

- (UIColor *)buttonsDelimeterColor{
    if ([self.delegate respondsToSelector:@selector(tabBarButtonsDelimeterColor:)])
        return [self.delegate tabBarButtonsDelimeterColor:self];
    return [UIColor grayColor];
}

- (NSArray *)tabBarConstraints{
    if ([self.delegate respondsToSelector:@selector(tabBarConstraints:)]) 
        return [self.delegate tabBarConstraints:self];
    return nil;
}

- (KATabBarOrientation)tabBarOrientation{
    if ([self.delegate respondsToSelector:@selector(tabBarOrientation:)])
        return [self.delegate tabBarOrientation:self];
    return KATabBarOrientationHorizontal;
}

- (double)delimiterWidth{
    if ([self.delegate respondsToSelector:@selector(tabBarDelimeterWidth:)])
        return [self.delegate tabBarDelimeterWidth:self];
    return 0;
}

- (void)resetSelection {
    for (UIButton *btn in _buttons) {
        [btn setSelected:NO];
    }
}

#pragma mark - Button pressed

- (void)tabBarButtonPressed:(UIButton *)button{
    NSInteger nSelectedIndex = button.tag - kButtonTagOffset;
    if (self.selectedIndex != nSelectedIndex) {
        [self setSelectedIndex:nSelectedIndex];
    } else {
        if ([self.delegate respondsToSelector:@selector(tabBarDidSelectCurrentViewController:)])
            [self.delegate tabBarDidSelectCurrentViewController:self];
    }
}

@end
