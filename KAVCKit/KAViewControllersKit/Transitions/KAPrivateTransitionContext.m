//
//  KAPrivateTransitionContext.m
//  Collaborate
//
//  Created by Anton Kovalev on 03.03.16.
//  Copyright © 2016 Rosberry. All rights reserved.
//

#import "KAPrivateTransitionContext.h"
#import "KANavigationController.h"
#import "KATabBarController.h"

@interface KAPrivateTransitionContext ()

@property (nonatomic) NSDictionary *privateViewControllers;
@property (nonatomic) KANavigationController *navigationController;
@property (nonatomic) CGRect privateDisappearingFromRect;
@property (nonatomic) CGRect privateAppearingFromRect;
@property (nonatomic) CGRect privateDisappearingToRect;
@property (nonatomic) CGRect privateAppearingToRect;
@property (nonatomic) UIView *containerView;
@property (nonatomic) UIModalPresentationStyle presentationStyle;
@property (nonatomic) BOOL transitionWasCancelled;
@property (nonatomic, readwrite) BOOL navigationBarInitiallyHidden;
@property (nonatomic, readwrite) BOOL navigationBarShouldBeHidden;
@property (nonatomic) BOOL transitionStarted;
@property (nonatomic) KANavigationControllerTransitionType transitionType;

@property (nonatomic) KATabBarController *tabBarController;

@property (nonatomic) BOOL needUpdateNavigationBarAtTheEnd;

@end

@implementation KAPrivateTransitionContext

@synthesize willCancelInteractiveTransitionBlock;

- (instancetype)initWithFromViewController:(UIViewController *)fromViewController toViewController:(UIViewController *)toViewController navigationController:(KANavigationController *)navigationController transitionType:(KANavigationControllerTransitionType)type navigationBarInitiallyHidden:(BOOL)initiallyHidden navigationBarShouldBeHidden:(BOOL)shouldBeHidden {
    self = [super init];
    if (self) {
        self.transitionType = type;
        self.navigationBarInitiallyHidden = initiallyHidden;
        self.navigationBarShouldBeHidden = shouldBeHidden;
        self.presentationStyle = UIModalPresentationCustom;
        self.containerView = fromViewController.view.superview;
        self.transitionWasCancelled = NO;
        self.privateViewControllers = @{UITransitionContextFromViewControllerKey:fromViewController,
                                        UITransitionContextToViewControllerKey:toViewController};
        self.navigationController = navigationController;
        
        self.privateDisappearingFromRect = [navigationController frameForViewController:fromViewController withNavigationBarHidden:initiallyHidden];
        self.privateAppearingToRect = [navigationController frameForViewController:toViewController withNavigationBarHidden:shouldBeHidden];
        self.privateAppearingFromRect = [navigationController frameForViewController:toViewController withNavigationBarHidden:shouldBeHidden];
        self.privateDisappearingToRect = [navigationController frameForViewController:fromViewController withNavigationBarHidden:initiallyHidden];
        
        double containerWidthOffset = CGRectGetWidth(self.containerView.bounds);
        double negativeOffset = -CGRectGetWidth(self.containerView.bounds)*.3f;
        
        if (type == KANavigationControllerTransitionTypePush) {
            self.privateAppearingFromRect = CGRectOffset(self.privateAppearingFromRect, containerWidthOffset, 0);
            self.privateDisappearingToRect = CGRectOffset(self.privateDisappearingToRect, negativeOffset, 0);
        } else if (type == KANavigationControllerTransitionTypePop) {
            self.privateDisappearingToRect = CGRectOffset(self.privateDisappearingToRect, containerWidthOffset, 0);
            self.privateAppearingFromRect = CGRectOffset(self.privateAppearingFromRect, negativeOffset, 0);
        }
    }
    return self;
}

- (instancetype)initWithFromViewController:(UIViewController *)fromViewController toViewController:(UIViewController *)toViewController tabBarController:(KATabBarController *)tabBarController {
    self = [super init];
    if (self) {
        self.presentationStyle = UIModalPresentationCustom;
        self.containerView = (id)tabBarController.viewControllersContainerView;
        self.transitionWasCancelled = NO;
        self.privateViewControllers = @{UITransitionContextToViewControllerKey:toViewController};
        if (fromViewController) {
            self.privateViewControllers = @{UITransitionContextFromViewControllerKey:fromViewController,
                                            UITransitionContextToViewControllerKey:toViewController};
        }
        self.tabBarController = tabBarController;
        self.privateDisappearingFromRect = self.privateAppearingToRect = self.containerView.bounds;
        self.privateDisappearingToRect = self.privateAppearingFromRect = self.containerView.bounds;
    }
    return self;
}

- (void)dealloc {
    self.privateViewControllers = nil;
    self.navigationController = nil;
    self.containerView = nil;
}

- (CGRect)initialFrameForViewController:(UIViewController *)viewController {
    if (viewController == [self viewControllerForKey:UITransitionContextFromViewControllerKey]) {
        return self.privateDisappearingFromRect;
    } else {
        return self.privateAppearingFromRect;
    }
}

- (CGRect)finalFrameForViewController:(UIViewController *)viewController {
    if (viewController == [self viewControllerForKey:UITransitionContextFromViewControllerKey]) {
        return self.privateDisappearingToRect;
    } else {
        return self.privateAppearingToRect;
    }
}

- (CGRect)initialFrameForNavigationBar {
    CGRect frame = [self.navigationController frameForNavigationBarHidden:self.navigationBarInitiallyHidden inViewController:[self viewControllerForKey:UITransitionContextFromViewControllerKey]];
    
    UIViewController *vc = nil;
    
    if (self.navigationBarShouldBeHidden && !self.navigationBarInitiallyHidden) {
        vc = [self viewControllerForKey:UITransitionContextFromViewControllerKey];
    } else if (self.navigationBarInitiallyHidden && !self.navigationBarShouldBeHidden) {
        vc = [self viewControllerForKey:UITransitionContextToViewControllerKey];
    }
    
    if (vc) {
        frame.origin.y = 0;
        frame.origin.x = [self initialFrameForViewController:vc].origin.x;
    }
    
    return frame;
}

- (CGRect)finalFrameForNavigationBar {
    CGRect navBarFrame = self.initialFrameForNavigationBar;
    
    UIViewController *vc = nil;
    
    if (self.navigationBarShouldBeHidden && !self.navigationBarInitiallyHidden) {
        vc = [self viewControllerForKey:UITransitionContextFromViewControllerKey];
    } else if (self.navigationBarInitiallyHidden && !self.navigationBarShouldBeHidden) {
        vc = [self viewControllerForKey:UITransitionContextToViewControllerKey];
    }
    
    if (vc) {
        navBarFrame.origin.x = [self finalFrameForViewController:vc].origin.x;
    }
    
    return navBarFrame;
}

- (UIViewController *)viewControllerForKey:(NSString *)key {
    return self.privateViewControllers[key];
}

- (UIView *)viewForKey:(NSString *)key{
    if ([key isEqualToString:UITransitionContextToViewKey]) {
        UIViewController *vc = self.privateViewControllers[UITransitionContextToViewControllerKey];
        return vc.view;
    } else if ([key isEqualToString:UITransitionContextFromViewKey]) {
        UIViewController *vc = self.privateViewControllers[UITransitionContextFromViewControllerKey];
        return vc.view;
    }
    return nil;
}

- (UIView *)navigationBar {
    return self.navigationController.navigationBar;
}

- (CGAffineTransform)targetTransform {
    return CGAffineTransformIdentity;
}

- (void)startTransitionWithDuration:(double)duration {
    if (!self.isAnimated) {
        duration = 0;
    }
    
    KANavigationController *navigationController = (id)[self navigationController];
    [navigationController.view setUserInteractionEnabled:NO];
    
    KATabBarController *tabBarController = [self tabBarController];
    [tabBarController.view setUserInteractionEnabled:NO];
    
    if (navigationController) {
        if (!self.navigationBarShouldBeHidden && self.navigationBarInitiallyHidden) {
            duration = 0;
        } else if (self.navigationBarShouldBeHidden && !self.navigationBarInitiallyHidden) {
            self.needUpdateNavigationBarAtTheEnd = YES;
        }
        if (!self.needUpdateNavigationBarAtTheEnd) {
            [self updateNavigationBarForNavigationController:navigationController withDuration:duration];
        }
    } else if (tabBarController) {
        [tabBarController.activeTabBar resetSelection];
        KANavigationController *tabBarControllerNavigationController = tabBarController.ka_navigationController;
        if (tabBarControllerNavigationController) {
            KANavigationBar *navigationBar = tabBarControllerNavigationController.navigationBar;
            [navigationBar needUpdateWithTransitionType:KANavigationControllerTransitionTypeTabBar withAnimationDuration:duration fromViewController:[self viewControllerForKey:UITransitionContextFromViewControllerKey] toViewController:[self viewControllerForKey:UITransitionContextToViewControllerKey]];
        }
    }
    
    self.transitionStarted = YES;
}

- (void)completeTransition:(BOOL)didComplete {
    NSAssert(self.transitionStarted, @"Transition must be started before completion. Call -startTransitionWithDuration: method");
    self.transitionStarted = NO;
    
    KANavigationController *navigationController = (id)[self navigationController];
    [navigationController.view layoutIfNeeded];
    
    KATabBarController *tabBarController = [self tabBarController];
    [tabBarController.view layoutIfNeeded];
    
    if (self.needUpdateNavigationBarAtTheEnd) {
        if (!self.transitionWasCancelled) {
            [self updateNavigationBarForNavigationController:navigationController withDuration:0];
        }
        self.needUpdateNavigationBarAtTheEnd = NO;
    }
    
    if (self.completionBlock) {
        self.completionBlock(didComplete);
    }
    
    [navigationController.view setUserInteractionEnabled:YES];
    [tabBarController.view setUserInteractionEnabled:YES];
}

- (void)updateInteractiveTransition:(CGFloat)percentComplete {
    
}

- (void)finishInteractiveTransition {
    self.transitionWasCancelled = NO;
}

- (void)cancelInteractiveTransition {
    self.navigationBarShouldBeHidden = NO;
    self.transitionWasCancelled = YES;
}

- (NSArray *)tabBars {
    return self.tabBarController.tabBars;
}

#pragma mark - Helper methods

- (void)updateNavigationBarForNavigationController:(KANavigationController *)navigationController withDuration:(double)duration {
    KANavigationBar *navigationBar = (id)[self navigationBar];
    [navigationBar needUpdateWithTransitionType:self.transitionType withAnimationDuration:duration fromViewController:[self viewControllerForKey:UITransitionContextFromViewControllerKey] toViewController:[self viewControllerForKey:UITransitionContextToViewControllerKey]];
    NSInteger count = self.navigationController.viewControllers.count;
    if (self.transitionType == KANavigationControllerTransitionTypePop) {
        if ([[self viewControllerForKey:UITransitionContextToViewControllerKey] isEqual:navigationController.rootViewController]) {
            count = 1;
        }
    }
    if (count > 1) {
        [navigationBar needShowBackButtonWithAnimationDuration:duration fromViewController:(id)[self viewControllerForKey:UITransitionContextFromViewControllerKey] toViewController:(id)[self viewControllerForKey:UITransitionContextToViewControllerKey]];
    } else {
        [navigationBar needHideBackButtonWithAnimationDuration:duration fromViewController:(id)[self viewControllerForKey:UITransitionContextFromViewControllerKey] toViewController:(id)[self viewControllerForKey:UITransitionContextToViewControllerKey]];
    }
}

@end
