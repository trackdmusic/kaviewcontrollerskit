//
//  KAViewControllerContextTransitioning.h
//  Collaborate
//
//  Created by Anton Kovalev on 01.03.16.
//  Copyright © 2016 Rosberry. All rights reserved.
//

@class KANavigationController, KATabBarController;
#import "KANavigationControllerTransitionType.h"

@protocol KAViewControllerContextTransitioning <NSObject, UIViewControllerContextTransitioning>

- (void)startTransitionWithDuration:(double)duration;

- (NSArray *)tabBars;
- (KATabBarController *)tabBarController;

- (UIView *)navigationBar;
- (KANavigationController *)navigationController;
- (KANavigationControllerTransitionType)transitionType;
- (CGRect)initialFrameForNavigationBar;
- (CGRect)finalFrameForNavigationBar;
@property (nonatomic, readonly) BOOL navigationBarInitiallyHidden;
@property (nonatomic, readonly) BOOL navigationBarShouldBeHidden;

@property (nonatomic, copy) void (^willCancelInteractiveTransitionBlock)();

@end
