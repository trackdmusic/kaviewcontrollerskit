//
//  KAPrivateTransitionContext.h
//  Collaborate
//
//  Created by Anton Kovalev on 03.03.16.
//  Copyright © 2016 Rosberry. All rights reserved.
//

#import <UIKit/UIKit.h>
#import <Foundation/Foundation.h>
#import "KAViewControllerContextTransitioning.h"
#import "KANavigationControllerTransitionType.h"
@class KANavigationController, KATabBarController;

@interface KAPrivateTransitionContext : NSObject <KAViewControllerContextTransitioning>

- (instancetype)initWithFromViewController:(UIViewController *)fromViewController toViewController:(UIViewController *)toViewController navigationController:(KANavigationController *)navigationController transitionType:(KANavigationControllerTransitionType)type navigationBarInitiallyHidden:(BOOL)initiallyHidden navigationBarShouldBeHidden:(BOOL)shouldBeHidden;

- (instancetype)initWithFromViewController:(UIViewController *)fromViewController toViewController:(UIViewController *)toViewController tabBarController:(KATabBarController *)tabBarController;

@property (nonatomic, copy) void (^completionBlock)(BOOL didComplete);
@property (nonatomic, assign, getter=isAnimated) BOOL animated;
@property (nonatomic, assign, getter=isInteractive) BOOL interactive;

@end
